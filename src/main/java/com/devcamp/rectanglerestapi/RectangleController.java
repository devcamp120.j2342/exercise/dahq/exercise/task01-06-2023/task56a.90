package com.devcamp.rectanglerestapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RectangleController {
    @GetMapping("/rectangle-area")
    public double Area(@RequestParam(required = true, name = "length") float paramLength,
            @RequestParam(required = true, name = "width") float paramWidth) {
        Rectangle rectangle = new Rectangle(paramLength, paramWidth);
        return rectangle.getArea();
    }

    @GetMapping("/rectangle-permiter")
    public double Permeter(@RequestParam(required = true, name = "length") float paramLength,
            @RequestParam(required = true, name = "width") float paramWidth) {
        Rectangle rectangle = new Rectangle(paramLength, paramWidth);
        return rectangle.getPerimeter();
    }

}
